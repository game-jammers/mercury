# mercury

## Bugs

## Features
 - [ ] [F001] Zambies
 - [ ] [F002] Shooting
 - [ ] [F003] Light
 - [ ] [F004] Tile variation system
 - [ ] [F005] SFX
 - [ ] [F006] Mission Objectives

## Assets
 - [ ] [A001] More variations
 - [ ] [A002] VFX 
    - [ ] [A002.1] Sparks
    - [ ] [A002.2] Smoke
    - [ ] [A002.3] Fog
 - [ ] [A003] Add ceilings
 - [ ] [A004] Textures

## Content

## Nits
 - [ ] [N001] More variation, smaller levels
