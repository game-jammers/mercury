//
// (c) LetsMakeGames 2021
// http://www.letsmake.games
//

using blacktriangles;
using UnityEngine;

namespace Mercury
{
    public class ActionSceneManager
        : SceneManager
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public Character playerPrefab;
        public Dungen.Generator generator;
				public Dungen.DungeonDefinition def;

        public Character player                                 { get; private set; } = null;

        //
        // unity callbacks ////////////////////////////////////////////////////
        //
        
        protected virtual void Start()
        {
						generator.Reset(def);
            generator.Build((generator,dungeon)=>{
                player = Instantiate(playerPrefab, new Vector3(0f, 2f, 0f), Quaternion.identity);
                RequestCamera(player);
                player.datamodel.inventory = GameManager.instance.player.loadout;
                player.Initialize();
            });
        }
        
    }
}
