//
// (c) GameJammers 2019
// https://jamming.games/
//

using blacktriangles;
using System.Collections.Generic;
using UnityEngine;

namespace Mercury
{
    public class Character
        : MonoBehaviour
        , ICameraController
    {
        //
        // types //////////////////////////////////////////////////////////////
        //
        
        public enum Type
        {
            Local,
            Remote,
            Doll
        }
        //
        // --------------------------------------------------------------------
        //
        
        public enum InitStyle
        {
            Manual,
            Awake,
            Start,
            SceneReady
        }
        
        //
        // members ////////////////////////////////////////////////////////////
        //

        public int pid                                          { get; private set; }
        public Type type                                        { get; private set; }
        public InitStyle initStyle                              = InitStyle.Manual;
        public bool initialized                                 { get; private set; }

        //
        // components ---------------------------------------------------------
        //
    
        public CharacterDatamodel datamodel                         { get; private set; }
        public CharacterControl controller                          { get; private set; }
        public CharacterView view                                   { get; private set; }
        public CharacterDriver driver                               { get; private set; }
        #if UNITY_EDITOR
            public new CharacterAudio audio                     { get; private set; }
        #else
            public CharacterAudio audio                         { get; private set; }
        #endif

    
        public IEnumerable<CharacterComponent> components
        {
            get
            {
                yield return datamodel;
                yield return controller;
                yield return driver;
                yield return view;
                yield return audio;
            }
        }
        
        //
        // ICameraController --------------------------------------------------
        //
    
        public btCamera cam                                         { get; private set; }
        public bool hasCamera                                       { get { return cam != null; } }
    
        //
        // constructor / initializer //////////////////////////////////////////
        //

        public static Character Spawn(Character prefab)
        {
            return Spawn(prefab, Vector3.zero, Quaternion.identity);
        }

        //
        // --------------------------------------------------------------------
        //


        public static Character Spawn(Character prefab, Transform spawn)
        {
            return Spawn(prefab, spawn.position, spawn.rotation);
        }

        //
        // --------------------------------------------------------------------
        //
        
        public static Character Spawn(Character prefab, Vector3 position, Quaternion rotation)
        {
            Character character = Instantiate(prefab, position, rotation) as Character;
            character.pid = -1;
            character.type = Type.Local;
            return character;
        }

        //
        // =-------------------------------------------------------------------
        //
        

        public static Character Spawn<DriverType>(Character prefab)
            where DriverType: CharacterDriver
        {
             Character res = Spawn(prefab);
             res.InstallDriver<DriverType>();
             return res;
        }
    
        //
        // --------------------------------------------------------------------
        //
        
        public void Initialize()
        {
            foreach(CharacterComponent component in components)
            {
                if(component != null)
                {
                    component.Initialize(this);
                }
            }
        }
    
        //
        // public methods /////////////////////////////////////////////////////
        //
        
        public void InstallDriver<DriverType>()
            where DriverType: CharacterDriver
        {
            driver = gameObject.AddComponent<DriverType>();
            driver.Initialize(this);
        }


        //
        // ICameraController //////////////////////////////////////////////////
        //

        public void OnTakeCamera(btCamera cam)
        {
            this.cam = cam;
            foreach(CharacterComponent component in components)
            {
                if(component is ICameraController cont)
                {
                    cont.OnTakeCamera(cam);
                }
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public void OnReleaseCamera()
        {
            this.cam = null;
            foreach(CharacterComponent component in components)
            {
                if(component is ICameraController cont)
                {
                    cont.OnReleaseCamera();
                }
            }
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //
        
        protected virtual void Awake()
        {
            datamodel = gameObject.GetComponent<CharacterDatamodel>();
            controller = gameObject.GetComponent<CharacterControl>();
            driver = gameObject.GetComponent<CharacterDriver>();
            view = gameObject.GetComponent<CharacterView>();
            audio = gameObject.GetComponent<CharacterAudio>();

            if(initStyle == InitStyle.Awake)
            {
                Initialize();
            }
        }
    
        //
        // --------------------------------------------------------------------
        //

        protected virtual void Start()
        {
            if(initStyle == InitStyle.Start)
            {
                Initialize();
            }
            else if(initStyle == InitStyle.SceneReady)
            {
                SceneManager.instance.OnSceneReady += ()=>{
                    Initialize();
                };
            }
        }
    }
}
