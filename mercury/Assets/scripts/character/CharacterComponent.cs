//
// (c) GameJammers 2019
// https://jamming.games/
//

using blacktriangles;
using UnityEngine;

namespace Mercury
{

    public class CharacterComponent
        : MonoBehaviour
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
    
        public bool initialized                                     { get; private set; }
        
        public Character character                                  { get; private set; }
        public CharacterDatamodel datamodel                         { get; private set; }
    
        //
        // constructor / initializer //////////////////////////////////////////
        //
        
        public virtual void Initialize(Character _character)
        {
            character = _character;
            datamodel = character.datamodel;
            initialized = true;
        }
    
        //
        // unity callbacks ////////////////////////////////////////////////////
        //
        
        protected virtual void Awake()
        {
            initialized = false;
        }
    }

}
