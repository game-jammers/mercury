//
// (c) GameJammers 2019
// https://jamming.games/
//

using blacktriangles;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mercury
{

    public class CharacterControl
        : CharacterComponent
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public UnityEngine.CharacterController unityController  = null;
        
        //
        // init ///////////////////////////////////////////////////////////////
        //

        public override void Initialize(Character character)
        {
            base.Initialize(character);

            if(unityController == null)
            {
                unityController = GetComponent<UnityEngine.CharacterController>();
            }

            DrawNext();
        }
        
        //
        // public methods /////////////////////////////////////////////////////
        //

        public void Move(Vector3 movedir)
        {
            datamodel.state.moveDir = movedir.normalized;
        }

        //
        // --------------------------------------------------------------------
        //

        public void Look(Vector2 look)
        {
            datamodel.state.lookDir += look * datamodel.stats.lookSpeed;
        }

        //
        // weapon control /////////////////////////////////////////////////////
        //

        public void DrawNext()
        {
            foreach(WeaponSlot slot in EnumUtility.GetValues<WeaponSlot>())
            {
                if(datamodel.inventory.GetWeaponData(slot) != null)
                {
                    Draw(slot, true);
                    return;
                }
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public void Draw(WeaponSlot slot, bool force  = false)
        {
            if(datamodel.state.animLock.isLocked) return;
            if(slot != datamodel.state.activeWeaponSlot || force)
            {
                StartCoroutine(DrawAsync(slot));
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public void Holster()
        {
            if(datamodel.state.animLock.isLocked) return;
            StartCoroutine(HolsterAsync());
        }

        //
        // --------------------------------------------------------------------
        //

        public void SetAim(bool aiming)
        {
            if(datamodel.state.activeWeapon != null)
            {
                if(aiming != datamodel.state.isAiming.val)
                {
                    datamodel.state.isAiming.val = aiming;
                    datamodel.state.activeWeapon.isAiming = datamodel.state.isAiming.val;
                }
            }
        }

        //
        // private methods ///////////////////////////////////////////////////
        //

        private IEnumerator DrawAsync(WeaponSlot slot)
        {
            datamodel.state.animLock.Lock();
            if(datamodel.state.activeWeapon != null)
                yield return HolsterAsync();

            WeaponData nextWeaponData = datamodel.inventory.GetWeaponData(slot);
            if(nextWeaponData != null)
            {
                Weapon weapon = nextWeaponData.Create();
                weapon.transform.SetParent(character.cam.transform);
                weapon.transform.localPosition = Vector3.zero;
                weapon.transform.localRotation = Quaternion.identity;
                datamodel.state.activeWeapon = weapon;
                yield return weapon.Draw(character);
            }
            datamodel.state.animLock.Unlock();
        }

        //
        // --------------------------------------------------------------------
        //

        private IEnumerator HolsterAsync()
        {
            datamodel.state.animLock.Lock();
            if(datamodel.state.activeWeapon != null)
            {
                yield return datamodel.state.activeWeapon.Holster();
                Destroy(datamodel.state.activeWeapon.gameObject);
                datamodel.state.activeWeapon = null;
            }
            datamodel.state.animLock.Unlock();
        }
        
        //
        // unity callbacks ////////////////////////////////////////////////////
        //
        
        protected virtual void Update()
        {
            if(!initialized) return;

            CharacterState state = datamodel.state;
            CharacterStats stats = datamodel.stats;

            Vector3 deltaMove = character.transform.TransformDirection(datamodel.state.moveDir * stats.moveSpeed);
            deltaMove += Physics.gravity;
            unityController.Move(deltaMove * Time.deltaTime);
            unityController.transform.localRotation = Quaternion.Euler(0f, state.lookDir.x, 0f);
            state.isGrounded.Set(unityController.isGrounded);
        }
    }

}
