//
// (c) GameJammers 2019
// https://jamming.games/
//

using blacktriangles;
using UnityEngine;

namespace Mercury
{

    public class CharacterDatamodel
        : CharacterComponent
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        [ReadOnly] public CharacterState state;                             
        public CharacterStats stats                             { get { return _stats; } }
        public Inventory inventory;

        [SerializeField] private CharacterStats _stats          = CharacterStats.Default;

        //
        // initialize /////////////////////////////////////////////////////////
        //
        
        public override void Initialize(Character _character)
        {
            base.Initialize(_character);
            state = new CharacterState(_character, stats);
        }
        
    }

}
