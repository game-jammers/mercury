//
// (c) GameJammers 2019
// https://jamming.games/
//

using blacktriangles;
using UnityEngine;

namespace Mercury
{
    [System.Serializable]
    public class CharacterState
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        public class TimestampedBool
            : TimestampedValue<bool>
        {
            public TimestampedBool(bool start)
                : base(start)
            {
            }
        }
        
        //
        // members ////////////////////////////////////////////////////////////
        //

        public Vector3 moveDir                                  = Vector3.zero;
        public Vector2 lookDir                                  = Vector2.zero;
        public bool isMoving                                    { get { return moveDir.sqrMagnitude > Mathf.Epsilon; } }
        public TimestampedBool isGrounded                       = new TimestampedBool(false);
        public TimestampedBool isAiming                         = new TimestampedBool(false);

        public LockStack animLock                               = new LockStack();

        public WeaponSlot activeWeaponSlot                      = WeaponSlot.Primary;
        public Weapon activeWeapon                              = null;

        //
        // constructor ////////////////////////////////////////////////////////
        //

        public CharacterState(Character character, CharacterStats stats)
        {
        }
    }
}
