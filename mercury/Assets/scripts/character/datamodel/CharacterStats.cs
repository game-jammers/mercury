//
// (c) GameJammers 2019
// https://jamming.games/
//

using blacktriangles;
using UnityEngine;

namespace Mercury
{
    [System.Serializable]
    public struct CharacterStats
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        [Header("MOVEMENT")]
        public float moveSpeed;
        public float lookSpeed;

        //
        // constructor ////////////////////////////////////////////////////////
        //
        
        public static CharacterStats Default 
        { 
            get 
            {
                return new CharacterStats() {
                    moveSpeed = 3f,
                    lookSpeed = 360f,
                };
            }
        }

    }
}
