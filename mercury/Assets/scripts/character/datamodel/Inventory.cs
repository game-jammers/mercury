//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using UnityEngine;

namespace Mercury
{
    [System.Serializable]
    public struct Inventory
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public WeaponData primary;
        public WeaponData secondary;
        public WeaponData melee;
        public WeaponData scanner;
        public EquipmentData armor;
        public EquipmentData utility;

        public int largeAmmo;
        public int smallAmmo;
        public int battery;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public WeaponData GetWeaponData(WeaponSlot slot)
        {
            switch(slot)
            {   
                case WeaponSlot.Primary: return primary;
                case WeaponSlot.Secondary: return secondary;
                case WeaponSlot.Melee: return melee;
                case WeaponSlot.Scanner: return scanner;
            }

            return null;
        }
    }
}
