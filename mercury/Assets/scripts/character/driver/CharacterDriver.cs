//
// (c) GameJammers 2019
// https://jamming.games/
//

using blacktriangles;
using UnityEngine;

namespace Mercury
{

    public class CharacterDriver
        : CharacterComponent
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
    
        public CharacterControl controller                          { get; private set; }
    
        //
        // ////////////////////////////////////////////////////////////////////
        //
        
        public override void Initialize(Character _character)
        {
            base.Initialize(_character);
            controller = character.controller;
        }
    }

}
