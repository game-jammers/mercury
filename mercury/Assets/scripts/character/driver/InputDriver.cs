//
// (c) {{company}} 2020
// {{url}}
//

using blacktriangles;
using UnityEngine;

namespace Mercury
{
    public class InputDriver
        : CharacterDriver
    {
        //
        // types //////////////////////////////////////////////////////////////
        //
        
        public enum Action
        {
            MoveX,
            MoveY,
            LookX,
            LookY,
            Aim,
        }

        //
        // members ////////////////////////////////////////////////////////////
        //

        private InputRouter<Action> input;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public override void Initialize(Character character)
        {
            base.Initialize(character);
            
            GameSettings.Mouse mouseSettings = GameManager.instance.settings.mouse;
            input = new InputRouter<Action>();
            input.Bind(Action.MoveX, InputAction.FromAxis(KeyCode.A, KeyCode.D));
            input.Bind(Action.MoveY, InputAction.FromAxis(KeyCode.S, KeyCode.W));
            input.Bind(Action.LookX, InputAction.FromAxis(InputAction.Axis.MouseHorizontal, mouseSettings.invertX, mouseSettings.speed.x));
            input.Bind(Action.LookY, InputAction.FromAxis(InputAction.Axis.MouseVertical, mouseSettings.invertY, mouseSettings.speed.y));
            input.Bind(Action.Aim, InputAction.FromKey(KeyCode.Mouse1));
        }

        //
        // --------------------------------------------------------------------
        //

        public virtual void Update()
        {
            if(!initialized) return;

            //
            // MOVE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //

            Vector3 move = new Vector3(
                input.GetAxis(Action.MoveX),
                0f,
                input.GetAxis(Action.MoveY));

            controller.Move(move);

            //
            // LOOK ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //

            Vector2 look = new Vector2(
                input.GetAxis(Action.LookX),
                input.GetAxis(Action.LookY)) * Time.deltaTime;

            controller.Look(look);

            controller.SetAim(input.GetKey(Action.Aim));
        }
        
        
        
    }
}
