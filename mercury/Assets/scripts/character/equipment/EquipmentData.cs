//
// (c) GameJammers 2021
// http://www.jamming.games
//

using UnityEngine;

namespace Mercury
{
    public abstract class EquipmentData
        : ScriptableObject
    {
        public abstract EquipmentType equipmentType             { get; }
    }
}
