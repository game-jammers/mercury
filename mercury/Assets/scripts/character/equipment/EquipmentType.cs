//
// (c) GameJammers 2021
// http://www.jamming.games
//

namespace Mercury
{
    public enum EquipmentType
    {
        Weapon,
        Armor,
        Utility
    }
}
