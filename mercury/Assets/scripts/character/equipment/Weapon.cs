//
// (c) LetsMakeGames 2021
// http://www.letsmake.games
//

using blacktriangles;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mercury
{
    public class Weapon
        : MonoBehaviour
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public Character character                              { get; private set; }
        public WeaponData data                                  { get; private set; }
        public Animator animator;

        public bool isAiming                                    = false;
        private double lastFired                                 = 0;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void Initialize(WeaponData _data)
        {
            data = _data;
            animator.SetFloat("aimspeed", data.aimSpeed);
        }

        //
        // --------------------------------------------------------------------
        //

        public bool Fire()
        {
            double now = Epoch.now;
            if(now - lastFired > data.shotDelay)
            {
                lastFired = now;
                OnFire();
								return true;
            }
						return false;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public virtual IEnumerator Draw(Character character)
        {
            this.character = character;
            character.datamodel.state.animLock.Lock();
            animator.SetFloat("drawspeed", data.drawSpeed);
            animator.Play("draw");
            yield return new WaitForSeconds(data.drawSpeed);
            character.datamodel.state.animLock.Unlock();
        }

        //
        // --------------------------------------------------------------------
        //

        public virtual IEnumerator Holster()
        {
            character.datamodel.state.animLock.Lock();
            animator.SetFloat("holsterspeed", data.holsterSpeed);
            animator.Play("holster");
            yield return new WaitForSeconds(data.holsterSpeed);
            character.datamodel.state.animLock.Unlock();
        }

        //
        // protected methods //////////////////////////////////////////////////
        //

        protected virtual void OnDrawn(Character character)
        {
        }

        //
        // --------------------------------------------------------------------
        //

        protected virtual void OnFire()
        {
            animator.SetTrigger("Fire");
        }
        
        //
        // --------------------------------------------------------------------
        //

        protected virtual void OnHolstered(Character character)
        {
        }

        //
        // --------------------------------------------------------------------
        //

        protected virtual void HitScanFire()
        {
        }

        //
        // --------------------------------------------------------------------
        //

        protected virtual void ProjectileFire()
        {   
        }
        
        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void Update()
        {
            animator.SetBool("isaiming", isAiming);
        }
    }
}
