//
// (c) LetsMakeGames 2021
// http://www.letsmake.games
//

using UnityEngine;

namespace Mercury
{
    [CreateAssetMenu(fileName="WeaponData", menuName="Mercury/Weapon Data")]
    public class WeaponData
        : EquipmentData
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public override EquipmentType equipmentType             { get { return EquipmentType.Weapon; } }
        public Weapon prefab;
        public WeaponSlot slot;
        public float drawSpeed                                  = 1f;
        public float holsterSpeed                               = 1f;
        public float aimSpeed                                   = 1f;
        public float reloadSpeed                                = 1f;

        public float shotsPerSecond               							= 2f;
        public float shotDelay                                  { get { return 1f / shotsPerSecond; } }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public Weapon Create()
        {
            Weapon result = Instantiate(prefab);
            result.Initialize(this);
            return result;
        }
    }    
}
