//
// (c) GameJammers 2021
// http://www.jamming.games
//

namespace Mercury
{
    public enum WeaponSlot
    {
        Primary,
        Secondary,
        Melee,
        Scanner,
    }
}
