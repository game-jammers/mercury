//
// (c) GameJammers 2019
// https://jamming.games/
//

using blacktriangles;
using UnityEngine;

namespace Mercury
{
    public class CharacterBody
        : MonoBehaviour
    {
        //
        // types //////////////////////////////////////////////////////////////
        //
        
        public Animator animator;

        //
        // --------------------------------------------------------------------
        //

        public enum Hardpoint
        {
            HandL,
            HandR,
            Head,
            Back,
            Chest,
            ThighL,
            ThighR,
            CalfL,
            CalfR
        };

        //
        // ------------------------------------------------------------------------
        //

        [System.Serializable]
        public class Hardpoints
            : EnumList<Hardpoint, Transform>
        {
        }

        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public Hardpoints hardpoints;
    }
}
