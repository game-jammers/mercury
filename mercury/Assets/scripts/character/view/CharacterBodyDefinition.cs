//
// (c) GameJammers 2019
// https://jamming.games/
//

using blacktriangles;
using System.Collections.Generic;
using UnityEngine;

namespace Mercury
{
    [CreateAssetMenu(fileName="CharacterBodyDefinition", menuName="blacktriangles/CharacterBody Definition")]
    public class CharacterBodyDefinition
        : ScriptableObject
    {
        public CharacterBodyParts parts;
    }
}
