//
// (c) GameJammers 2019
// https://jamming.games/
//

using blacktriangles;
using System.Collections.Generic;
using UnityEngine;

namespace Mercury
{
    public class CharacterBodyPart
        : MonoBehaviour
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        public enum Type
        {
            Head,
            Torso,
            Hands,
            Feet,
            Attachment,
        };

        //
        // members ////////////////////////////////////////////////////////////
        //

        public Type type;
        public CharacterBody rig;
        public ColorScheme colors;

        public bool isAttachment                                { get { return type == Type.Attachment; } }

        [ShowIf("isAttachment")]
        public CharacterBody.Hardpoint attachPoint;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void ApplyColors(Material material)
        {
            material.SetColor("_Color0", colors.color0);
            material.SetColor("_Color1", colors.color1);
            material.SetColor("_Color2", colors.color2);
            material.SetColor("_Color3", colors.color3);
            material.SetColor("_Color4", colors.color4);
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void ApplyColors()
        {
            List<Material> mats = gameObject.GetAllMaterials();
            foreach(Material mat in mats)
            {
                ApplyColors(mat);
            }
        }

        //
        // ------------------------------------------------------------------------
        //

        public List<Material> GetSharedMaterials()
        {
            return gameObject.GetAllSharedMaterials();
        }

        #if UNITY_EDITOR
        [ContextMenu("Apply Colors")]
        public void EDITORONLY_ApplySharedColors()
        {
            List<Material> materials = GetSharedMaterials();
            foreach(Material mat in materials)
            {
                ApplyColors(mat);
            }
        }
        #endif
    }
}
