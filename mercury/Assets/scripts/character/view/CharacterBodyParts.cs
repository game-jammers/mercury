//
// (c) GameJammers 2019
// https://jamming.games/
//

using blacktriangles;
using System.Collections.Generic;
using UnityEngine;

namespace Mercury
{
    [System.Serializable]
    public class CharacterBodyParts
        : EnumList<CharacterBodyPart.Type, CharacterBodyPart>
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public CharacterBody rig                                { get { return GetRig(); } }
        
        //
        // public methods /////////////////////////////////////////////////////
        //

        public void Use(CharacterBodyPart part)
        {
            if(part != null)
            {
                this[part.type] = part; 
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void Use(CharacterBodyParts parts)
        {
            if(parts != null)
            {
                parts.ForEach((type, part)=>{
                    Use(part);
                });
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        private CharacterBody GetRig()
        {
            CharacterBodyPart part = first;
            if(part == null) return null;
            return part.rig;
        }
        
    }
}
