//
// (c) GameJammers 2020
// https://jamming.games/
//

using blacktriangles;
using UnityEngine;

namespace Mercury
{
    [System.Serializable]
    public struct ColorScheme
    {
        public Color color0; // key code black
        public Color color1; // key code red
        public Color color2; // key code green
        public Color color3; // key code blue
        public Color color4; // key code white
    };
}
