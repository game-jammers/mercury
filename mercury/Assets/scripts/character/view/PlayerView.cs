//
// (c) GameJammers 2019
// https://jamming.games/
//

using blacktriangles;
using UnityEngine;

namespace Mercury
{
    public class PlayerView
        : CharacterView
        , ICameraController
    {
        //
        // members ////////////////////////////////////////////////////////////////
        //
        
        public Animator animator                                { get { return datamodel.state.activeWeapon == null ? null : datamodel.state.activeWeapon.animator; } }
        public Transform cameraTransform                        = null; 

        public btCamera cam                                     { get; private set; } = null;
        public bool hasCamera                                   { get { return cam != null; } }

        //
        // ICameraController //////////////////////////////////////////////////
        //
    
        public void OnTakeCamera(btCamera cam)
        {
            cam.transform.SetParent(cameraTransform);
            cam.transform.localPosition = Vector3.zero;
            cam.transform.localRotation = Quaternion.identity;
        }

        //
        // --------------------------------------------------------------------
        //

        public void OnReleaseCamera()
        {
            cam = null;
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void Update()
        {
            if(!initialized) return;

            CharacterState state = datamodel.state;
            CharacterStats stats = datamodel.stats;

            state.lookDir.x = state.lookDir.x % 360f;
            state.lookDir.y = btMath.Clamp(state.lookDir.y, -90f, 90f);
            cameraTransform.localRotation = Quaternion.Euler(state.lookDir.y, 0f, 0f);
        }
    }
}
