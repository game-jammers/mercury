//
// (c) GameJammers 2019
// https://jamming.games/
//

using blacktriangles;
using UnityEditor;

namespace Mercury
{
    public class ExampleDb
        : DatabaseTable
    {
        //
        // members/////////////////////////////////////////////////////////////////
        //
        
        public GameManager[] managers         									{ get; private set; }
        
        //
        // public methods /////////////////////////////////////////////////////////
        //
    
        public override void Initialize(Database db)
        {
           managers = LoadAll<GameManager>("managers"); 
        }
    }
}
