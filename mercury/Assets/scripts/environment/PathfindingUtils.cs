//
// (c) GameJammers 2019
// https://jamming.games/
//

#if AGRANBURG_ASTAR

using blacktriangles;
using Pathfinding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mercury
{
    public static class Utils
    {
        //
        // public methods /////////////////////////////////////////////////////
        //

        public static List<Mesh> CollectGroundMeshes()
        {
            Mesh[] allGos = GameObject.FindObjectsOfType<Mesh>();
            List<Mesh> result = allGos.Filter((Mesh m)=>{
                return false;
            });

            DebugUtility.Log(result.Count.ToString());
            return result;
        }
    }
}

#endif
